#include "main.h"
#include "SPI.h"

SPI_HandleTypeDef hspi3;
uint8_t txbuf[MAX_SIZE]="force requiment";
uint8_t rxbuf[3]={0};



void Init_FCU_SPI(void)
{
	SPI3_Init();
	GPIO_Init_SPI3();
}

void SPI3_Init(void)
{
	hspi3.Instance = SPI3;
	hspi3.Init.Mode = SPI_MODE_MASTER;
	hspi3.Init.Direction = SPI_DIRECTION_2LINES;
	hspi3.Init.DataSize = SPI_DATASIZE_8BIT;
	hspi3.Init.CLKPolarity = SPI_POLARITY_LOW;
	hspi3.Init.CLKPhase = SPI_PHASE_1EDGE;
	hspi3.Init.NSS = SPI_NSS_SOFT;
	hspi3.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
	hspi3.Init.FirstBit = SPI_FIRSTBIT_MSB;
	hspi3.Init.TIMode = SPI_TIMODE_DISABLE;
	hspi3.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	hspi3.Init.CRCPolynomial = 10;
	if (HAL_SPI_Init(&hspi3) != HAL_OK)
	{
		Error_Handler();
	}
}

void GPIO_Init_SPI3(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOE_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();

	HAL_GPIO_WritePin(GPIOA, SS_FCU|LED_A, GPIO_PIN_SET);

	GPIO_InitStruct.Pin = LED_A|SS_FCU;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	HAL_GPIO_WritePin(GPIOE, LED_B, GPIO_PIN_SET);

	GPIO_InitStruct.Pin = LED_B;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
}


// transmit ili receive da pravim funkcije u spi.c a u main-u samo da ih pozivam
void fcu_spi_transmit(){

	HAL_GPIO_WritePin(GPIOA, SS_FCU, GPIO_PIN_RESET);

	if(HAL_SPI_Transmit(&hspi3, txbuf,MAX_SIZE, 1000) == HAL_OK)
		HAL_GPIO_WritePin(GPIOA, LED_A, GPIO_PIN_RESET); //AKO JE OK UPALI A6
		HAL_Delay(250);

	HAL_GPIO_WritePin(GPIOA, SS_FCU, GPIO_PIN_SET);

}

void fcu_spi_receive(){

	HAL_GPIO_WritePin(GPIOA, SS_FCU, GPIO_PIN_RESET);

	if(HAL_SPI_Receive(&hspi3, rxbuf,sizeof(rxbuf),1000)==HAL_OK)
	{
		HAL_GPIO_WritePin(GPIOE, LED_B, GPIO_PIN_RESET); //OK JE UPALI E15
		HAL_Delay(250);
	}

	HAL_GPIO_WritePin(GPIOA, SS_FCU, GPIO_PIN_SET);
}





