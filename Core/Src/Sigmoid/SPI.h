#define MAX_SIZE (20)
#define SS_FCU   (GPIO_PIN_4)  // on port A
#define SCLK_FCU (PIO_PIN_10)  // on port C
#define MISO_FCU (GPIO_PIN_11) // on port C
#define MOSI_FCU (GPIO_PIN_12) // on port C
#define LED_A    (GPIO_PIN_6)  // on port A
#define LED_B    (GPIO_PIN_15) // on port A

void Init_FCU_SPI(void);
void GPIO_Init_SPI3(void);
void SPI3_Init(void);
void fcu_spi_transmit(void);
void fcu_spi_receive();
void fcu_full_duplex(void);
